const express = require('express');
const playerController = require('../controller/player');
const router = express.Router();
router.get('/', playerController.all);
router.get('/add', playerController.createPlayerPage);
router.post('/add', playerController.savePlayer);
router.get('/edit/:id',playerController.edit)
router.post('/edit/:id',playerController.update)
router.get('/delete/:id',playerController.delete)
module.exports = router