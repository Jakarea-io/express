require('./database/connection')
const express = require('express')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const path = require('path');
const web= require('./routes/web')

const app = express();
const port = 5000;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs'); // configure template engine

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder
app.use(fileUpload()); // configure fileupload

app.use("/", web);
app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});