'use strict';

let mongoose = require('mongoose');

let Schema = mongoose.Schema;

var dataSchema = new Schema({
    firstName: String,
    lastName: String
});

let dataModel = mongoose.model('data', dataSchema);
module.exports = dataModel;
